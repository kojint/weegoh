import React from 'react';
import welcome from '../welcome.svg';
//import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Welcome(props) {
    return (
        <>
        <Col className="text-left">
            <Row className="welcome">
                <Col sm={12} md={6} className="text-left d-flex align-items-center">
                <div className="">
                    <h1>Welcome to weegoh!</h1>
                    <h4>We help you go where you need to around Gaborone! Pick a stop below.</h4>
                </div>
                </Col>
                <Col sm={12} md={6} className="text-left">
                <img src={welcome} className="mr-2" alt="welcome" width="100%" />
                </Col>
            </Row>
        </Col>
        </>
    );
}

export default Welcome;
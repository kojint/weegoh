import React from 'react';
import logo from '../logo.svg';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

function Navigation(props) {
    return (
        <>
            <Container fluid="true" className="navbar-wrapper">
                <Container className="">
                    <Row className="pt-2 pb-2">
                        <Navbar collapseOnSelect expand="lg" variant="light" className="w-100">
                            <Navbar.Brand href="#home">
                                <img src={logo} className="mr-2" alt="logo" height="20px" />
                                weegoh
                            </Navbar.Brand>
                            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                            <Navbar.Collapse id="responsive-navbar-nav">
                                <Nav className="ml-auto">
                                    <Nav.Link href="#stops">Stops</Nav.Link>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                    </Row>
                </Container>
            </Container>
        </>
    );
}

export default Navigation;
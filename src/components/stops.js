import React from 'react';
import axios from 'axios';
import config from '../config';
import Col from 'react-bootstrap/Col';

class Stops extends React.Component {

  state = {
    stops: [],
    currentStop: {},
    agency: {},
    coordinates: [],
    modes: [],
    active: false
  };

  componentDidMount() {
    this.getToken();
    console.log("API Token Received");
  }

  componentWillUnmount() {
  }

  getToken() {
    const self = this;
    let querystring = require("querystring");
    axios
      .post(
        "https://identity.whereismytransport.com/connect/token",
        querystring.stringify({
          client_id: config.client_id,
          client_secret: config.client_secret,
          grant_type: config.grant_type,
          scope: config.scope
        })
      )
      .then(function (response) {
        //success action
        self.accessToken = response.data.access_token; // Log the Access Token
        self.getStops();
      })
      .catch(function (error) {
        //error action
        console.log(error);
      });
  };

  getStops() {
    const self = this;
    axios
      .get(
        "https://platform.whereismytransport.com/api/stops?agencies=" + config.agencies,
        {
          headers: {
            Authorization: "Bearer " + self.accessToken,
            "Content-Type": "application/json",
            Accept: "application/json"
          }
        }
      )
      .then(function (response) {
        //success action
        self.setState({ stops: response.data });
        console.log(response.data);
        self.getStopDetails(response.data[0]); // set initial details from 1st stop
      })
      .catch(function (error) {
        //error action
        console.log(error);
      });
  };

  getStopDetails(stop) {
    // Set stop vars in state for binding

    this.setState({ currentStop: stop });
    this.setState({ agency: stop.agency });
    this.setState({ coordinates: stop.geometry.coordinates });
    this.setState({ modes: stop.modes });
    console.log(stop);

    this.toggleActive();
  };

  toggleActive() {
    this.setState({ active: !this.state.active });
  }

  render() {
    return (
      <>
        <Col sm={12} md={6} className="mb-4">
          <Col md={12} className="text-left section-wrapper">
            <div className="d-flex justify-content-between">
              <h3 className="mb-4">Stops</h3>
              <p className="mb-4 small pt-2">Scroll for more</p>
            </div>
            <div className="stops-list">
              {this.state.stops.map((stop, i) => {
                return <div className={this.state.currentStop.id === stop.id ? 'active' : ''} key={i}><div className="stop-item" onClick={(e) => this.getStopDetails(stop, e)}><p>{i + 1}. {stop.name}</p></div></div>
              })}
            </div>
          </Col>
        </Col>
        <Col sm={12} md={6} className="mb-4">
          <Col md={12} className="text-left section-wrapper card-sticky">
            <div>
              <h3 className="mb-4">{this.state.currentStop.name}</h3>
            </div>
            <h6>
              <div className="d-flex justify-content-between"><h5 className="mr-2">Agency:</h5><p>{this.state.agency.name}</p></div>
              <div className="d-flex justify-content-between"><h5 className="mr-2">Latitude:</h5><p>{this.state.coordinates[1]}</p></div>
              <div className="d-flex justify-content-between"><h5 className="mr-2">Longitude:</h5><p>{this.state.coordinates[0]}</p></div>
              <div className="d-flex justify-content-between"><h5 className="mr-2 mb-4">Modes:</h5>
                <div>
                  {this.state.modes.map((mode, i) => {
                    return <p className="ml-2" key={i}>{i + 1}. {mode}</p>
                  })}
                </div>
              </div>
              <div className="d-flex justify-content-between">
                <div></div>
                <div>
                  <a href="https://www.whereismytransport.com/">
                    <button className="btn btn-primary">See more</button>
                  </a>
                </div>
              </div>
            </h6>
          </Col>
        </Col>
      </>
    );
  }
}

export default Stops;


import React from 'react';
import logo from '../logo.svg';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Footer(props) {
    return (
        <>
            <Container fluid="true" className="bg-light">
                <Row className="">
                    <Col sm={12} md={4}>2019 © Thabang Chukura</Col>
                    <Col sm={12} md={4}>
                        <img src={logo} className="mr-2" alt="logo" height="20px" />
                    </Col>
                    <Col sm={12} md={4}>
                        <a href="https://www.whereismytransport.com/">
                            Data Provider
                    </a>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default Footer;
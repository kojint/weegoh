import React from 'react';
import './App.css';
import Navigation from './components/navgation';
import Footer from './components/footer';
import Stops from './components/stops';
import Welcome from './components/welcome';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

function App() {
  return (
    <div className="App">
      <header>
        <Navigation sticky="top" />
      </header>
      <main className="main">
        <Container fluid="true" className="">
          <Container className="">
            <Row className="pt-2 pb-2 mt-4 mb-4 welcome-wrapper">
              <Welcome />
            </Row>
            <Row className="pt-2 pb-2 mb-4" id="stops">
              <Stops />
            </Row>
          </Container>
        </Container>
      </main>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default App;
